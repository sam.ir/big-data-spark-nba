#!/usr/bin/env python
"""
    Jobs

    Root Package for all Jobs
"""

__author__ = "Samir Makhani"
__copyright__ = "Copyright 2018, legal@ui.agency"
__credits__ = ["Samir Makhani"]
__license__ = "GPL-V3.0"
__version__ = "0.1.0"
__maintainer__ = "Samir Makhani"
__email__ = "samir@ui.agency"

from src.jobs.espn.daily_leaders_job import DailyLeadersJob
from src.jobs.espn.daily_stories_job import DailyStoriesJob

__all__ = [
    # ESPN.com
    "DailyLeadersJob",
    "DailyStoriesJob"

    # NBA.com
]
