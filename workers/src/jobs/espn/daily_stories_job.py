#!/usr/bin/env python
"""
    Scraper Job The gathers front page
    stories from the /nba url and packages 
    them into an avro.
    
    Job Schedule: 
        Every Day
        at 12:00 PM, 3:00 PM, 8:00 PM, 12:00 AM
"""

__author__ = "Samir Makhani"
__copyright__ = "Copyright 2018, legal@ui.agency"
__credits__ = ["Samir Makhani"]
__license__ = "GPL-V3.0"
__version__ = "0.1.0"
__maintainer__ = "Samir Makhani"

from src.jobs.selenium_job import SeleniumJob
from src.sched.cronjob import cronjob
from src.utils.logger import get_logger
from src.utils.webdriver_util import DriverUtil
from src.utils.avro_utils import AvroUtils

from datetime import datetime
import time


logger = get_logger(__name__)

@cronjob(lambda job: (\
    job.hour.on(0,12,15,20),\
    job.minute.on(0)))
class DailyStoriesJob(SeleniumJob):

    def run(self, spark):
        """
            Navigate to espn.com/nba front page and gather the
            headline stories one by one and store them in an avro
            file based on the time the story was retrieved.

            :param: spark - SparkSession generated by BaseJob
        """
        self.driver.get('http://espn.com/nba')
        logger.info('Navigated to espn.com/nba')
        time.sleep(5)

        story_fields = ['title', 'summary', 'link', 'date']
        logger.info('Field Names for espn.com/nba stories: [ title, summary, link, date ]')

        stories = DriverUtil.find_by_xpath(self.driver, '//section[contains(@class, "contentItem__content")]')
        logger.info('Found %s stories from espn.com/nba' % str(len(stories)))

        story_data = []
        dt_str = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        for story in stories:
            links = DriverUtil.find_by_xpath(story, './/a')

            if len(links) > 0:
                data = self.extract_story_info(links[0], dt_str)
                if data is not None:
                    story_data.append(data)

        if len(story_data) > 0:
            story_df = spark.createDataFrame(story_data, story_fields)
            AvroUtils.avro_to_hdfs(story_df, '/stories/')


    def extract_story_info(self, story_link, dt_str):
        """
            Extract the Title, Summary and Link of a story based on the 
            element provided.

            :param: story_link - the parent element that hold the info
            :param: dt_str - the date string that represents the time this story was retrieved.
        """
        titles = DriverUtil.find_by_xpath(story_link, './/h1[contains(@class, "contentItem__title")]')
        summaries = DriverUtil.find_by_xpath(story_link, './/p[contains(@class, "contentItem__subhead")]')

        if len(titles) > 0 and len(summaries) > 0:
            title = titles[0].get_attribute('innerHTML')
            summary = summaries[0].get_attribute('innerHTML')
            link = story_link.get_attribute('href')
            return [title, summary, link, dt_str]
        else:
            return None

