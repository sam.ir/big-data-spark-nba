#!/usr/bin/env python
"""
    Utils>>Root package for all Utility Classes and Functions
"""

__author__ = "Samir Makhani"
__copyright__ = "Copyright 2018, legal@ui.agency"
__credits__ = ["Samir Makhani"]
__license__ = "GPL-V3.0"
__version__ = "0.1.0"
__maintainer__ = "Samir Makhani"
__email__ = "samir@ui.agency"

from src.utils.config_utils import *

__all__ = [
    "get_config_string", "get_config_float", "get_config_int",
    "get_config_item", "get_config_bool"
]