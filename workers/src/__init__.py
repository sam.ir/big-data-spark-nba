#!/usr/bin/env python
"""
    NBA Big Data, Trends & Analysis Sample Project by [UI.Agency](ui.agency) Source Code Package Main Root Directory
"""

__author__ = "Samir Makhani"
__copyright__ = "Copyright 2018, legal@ui.agency"
__credits__ = ["Samir Makhani"]
__license__ = "GPL-V3.0"
__version__ = "0.1.0"
__maintainer__ = "Samir Makhani"
__email__ = "samir@ui.agency"
