#!/usr/bin/env python
"""
    @cronjob

    Decorator for assigning spark jobs with a schedule
    configuration
"""

__author__ = "Samir Makhani"
__copyright__ = "Copyright 2018, legal@ui.agency"
__credits__ = ["Samir Makhani"]
__license__ = "GPL-V3.0"
__version__ = "0.1.0"
__maintainer__ = "Samir Makhani"
__email__ = "samir@ui.agency"

from src.utils.logger import get_logger
from src.sched.sched_context import SchedContext

logger = get_logger(__name__)


def cronjob(schedule=None):
    """
    Add a Job to the crontab scheduler. 
    Provide a lambda function that will set 
    job scheduling attributes.

    By default, when no schedule runs every minute of every day by default.
    """
    def real_decorator(job):
        """Get Crontab Context and add job"""
        try:
            SchedContext() \
                .remove_job(job.__name__) \
                .add_job(job, sched=schedule)
        except Exception as err:
            logger.error('Error initializing Crontab.')
            logger.error(err.args)
        return job
    return real_decorator
        