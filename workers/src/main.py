#!/usr/bin/env python
"""
    Main File for Executing Spark Jobs
"""

__author__ = "Samir Makhani"
__copyright__ = "Copyright 2018, legal@ui.agency"
__credits__ = ["Samir Makhani"]
__license__ = "GPL-V3.0"
__version__ = "0.1.0"
__maintainer__ = "Samir Makhani"
__email__ = "samir@ui.agency"

import subprocess
import sys

if __name__ == "__main__":
    """
        Execute the specified job within the module provided.
    """
    subprocess.call(['python', '-m', 'src.executor', sys.argv[1]], cwd='/root/nbaTrends')