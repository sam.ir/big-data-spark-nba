#!/usr/bin/env python
"""
    Main File for NBA Big Data, Trends & Analysis Sample Project by UI.Agency
    Starts the application and configures Crontab with all
    available jobs.
"""

__author__ = "Samir Makhani"
__copyright__ = "Copyright 2018, legal@ui.agency"
__credits__ = ["Samir Makhani"]
__license__ = "GPL-V3.0"
__version__ = "0.1.0"
__maintainer__ = "Samir Makhani"
__email__ = "samir@ui.agency"

import src.jobs as jobs
from src.sched.sched_context import SchedContext

if __name__ == "__main__":
    """
        Gather all jobs from job context and setup crontab
        By instantiating the class object, the cron context
        will be updated to create/update job context
    """
    for job in jobs.__all__:
        j = getattr(jobs, job)
        j()