FROM docker.elastic.co/beats/filebeat:6.2.2

USER root

RUN yum clean all; \
    rpm --rebuilddb; \
    yum install -y curl which tar sudo openssh-server openssh-clients rsync

RUN yum -y update

RUN yum -y install yum-utils
RUN yum -y install https://$(rpm -E '%{?centos:centos}%{!?centos:rhel}%{rhel}').iuscommunity.org/ius-release.rpm

RUN ssh-keygen -q -N "" -t dsa -f /etc/ssh/ssh_host_dsa_key
RUN ssh-keygen -q -N "" -t rsa -f /etc/ssh/ssh_host_rsa_key
RUN ssh-keygen -q -N "" -t rsa -f /root/.ssh/id_rsa
RUN cp /root/.ssh/id_rsa.pub /root/.ssh/authorized_keys

# Python 3
RUN yum -y install python36u
RUN yum -y install python36u-pip
RUN pip3.6 install virtualenv

# Crontab 
RUN yum -y install cronie

# Java 1.8
RUN curl -v -j -k -L -O -H 'Cookie: oraclelicense=accept-securebackup-cookie' http://download.oracle.com/otn-pub/java/jdk/8u161-b12/2f38c3b165be4555a1fa6e98c45e0808/jdk-8u161-linux-x64.rpm
RUN rpm -i jdk-8u161-linux-x64.rpm
RUN rm -f jdk-8u161-linux-x64.rpm

ENV JAVA_HOME /usr/java/default
ENV PATH $PATH:$JAVA_HOME/bin
RUN rm /usr/bin/java && ln -s $JAVA_HOME/bin/java /usr/bin/java

# Spark 2.3
RUN curl -v -j -k -L -O 'http://apache.claz.org/spark/spark-2.3.0/spark-2.3.0-bin-hadoop2.7.tgz'
RUN tar -xzf spark-2.3.0-bin-hadoop2.7.tgz
RUN cp -R spark-2.3.0-bin-hadoop2.7 /usr/local
RUN rm -rf spark-2.3.0-bin-hadoop2.7
RUN rm -f spark-2.3.0-bin-hadoop2.7.tgz
RUN cd /usr/local && ln -s ./spark-2.3.0-bin-hadoop2.7 /usr/local/spark
ENV PATH $PATH:/usr/local/spark/bin

# Add Spark Dependencies
RUN mkdir -p /usr/local/spark/addtl_jars
ADD ./docker/jars/* /usr/local/spark/addtl_jars

RUN mkdir /root/scripts
ADD ./docker/entrypoint-spark.sh /root/scripts/entrypoint.sh
RUN chmod +x /root/scripts/entrypoint.sh

# chrome
RUN curl -v -j -k -L -O 'https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm'
RUN yum install -y ./google-chrome-stable_current_x86_64.rpm
RUN rm -f google-chrome-stable_current_x86_64.rpm
RUN yum install -y zip unzip
RUN curl -v -j -k -L -O https://chromedriver.storage.googleapis.com/2.37/chromedriver_linux64.zip
RUN unzip chromedriver_linux64.zip -d /opt/chromedriver
RUN ln -s /opt/chromedriver/chromedriver /usr/bin/chromedriver

RUN ln -sfn /usr/bin/python3.6 /usr/bin/python
WORKDIR /root/nbaTrends

# spark ports
EXPOSE 8080 7077

ENTRYPOINT ["/root/scripts/entrypoint.sh"]